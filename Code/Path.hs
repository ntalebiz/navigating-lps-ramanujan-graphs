-- given an integral solution an integral solution (x1,x2,x3,x4)\in V_N as an input, 
-- this module returns the associated path with the generator set 
-- s_1,...,s_{p+1}. Each of of them has norm p. This is an easy task.


-- Input: 

-- an integral solution (x1,x2,x3,x4)\in V_p^h


-- Output:
-- a work of length h with s_1 ,,, s_(p+1)


module Path where
import Quantum.Synthesis.EuclideanDomain
import Quantum.Synthesis.Diophantine
import Quantum.Synthesis.Ring



{-This function is basically the quaternion multiplication-}

mult :: (ZComplex,ZComplex)->(ZComplex,ZComplex) -> (ZComplex,ZComplex)
mult (z,w) (z1,w1) = (z*z1-(w*(adj w1)), z*w1+(w*(adj z1)))



divs5 :: (ZComplex,ZComplex)->Bool
divs5 (Cplx a b,Cplx c d)= ((mod a 5)==0) && ((mod b 5)==0) && ((mod c 5)==0) && ((mod d 5)==0)

div5 :: (ZComplex,ZComplex)->(ZComplex,ZComplex)
div5 (Cplx a b,Cplx c d) = (Cplx (euclid_div a 5) (euclid_div b 5),Cplx (euclid_div c 5) (euclid_div d 5))


decompose :: (ZComplex,ZComplex) -> String
decompose (z,w)
	| divs5 (mult (Cplx 1 2, 0) (z,w) )= "I^{-1}"++ (decompose $ div5 $mult (Cplx 1 2, 0) (z,w) )
	| divs5 (mult (Cplx 1 (-2), 0) (z,w))= 'I':(decompose $ div5 $ mult (Cplx 1 (-2), 0) (z,w))
	| divs5 (mult (1, Cplx 2 0) (z,w) )= "J^{-1}"++(decompose $ div5 $ mult (1, Cplx 2 0) (z,w))
	| divs5 (mult (1, Cplx (-2) 0) (z,w) )= 'J':(decompose $ div5 $ mult (1, Cplx (-2) 0) (z,w))
	| divs5 (mult (1, Cplx 0 2) (z,w) )= "K^{-1}"++(decompose $ div5 $ mult (1, Cplx 0 2) (z,w))
	| divs5 (mult (1, Cplx 0 (-2)) (z,w) )= 'K':(decompose $ div5 $ mult (1, Cplx 0 (-2)) (z,w))
	| otherwise = []




