-- In this module we define all the options for the output the suitable options are
-- -p for changing the prime for generators
-- -q mod for the approximation mod q
-- -n hardness of factorization
-- -a gives us the minimal integral lift
-- -s gives us the statistics of the algorithm


module Main