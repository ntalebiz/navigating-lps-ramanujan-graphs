-- Given a prime number p we generate all the points on the sphere of radius sqrt{p}
-- This gives us a set of generators for the Cayley graph X_{p,q}

-- Input: 
-- prime number p


-- Output:
-- p+1 matrices of norm p





module Generators