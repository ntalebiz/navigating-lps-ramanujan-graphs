-- -- In this module given a local solution (a,b,0,0) mod q.
-- -- We find an integral solution (x1,x2,x3,x4)\in V_p^h which approximate our local solution.
-- hardness of factorization is another parameter of this module.

-- Inputs:  

-- -factor :hardness of factorization
-- -v  :vector (a,b,0,0) mod q 
-- -q  : mod q



-- Output
-- an integral solution (x1,x2,x3,x4)\in V_p^h
-- -p^h: the level set for the integral lift



-- functions:
--factorization
-- An LLL algorithm for lattice reduction gaussreduction 
-- sum of two squares
 
 
module Approximation where
import LLL
import Quantum.Synthesis.EuclideanDomain
import Quantum.Synthesis.Diophantine
import Quantum.Synthesis.Ring
import Quantum.Synthesis.StepComp
import System.Random
import Data.Maybe
import Data.Function
import Data.List.Split (splitOn)
import Data.Number.FixedPrec
import Quantum.Synthesis.GridSynth
import Quantum.Synthesis.Ring
import Control.Exception


{-
Given an integer a, this function returns a^-1 mod q. Tested !
-}

inv' :: EuclideanDomain a => a -> a -> a
inv' q a = fromJust $ inv_mod q a




{- 
In this function we introduce a lattice corresponded to (a,b) mod q.
Later, we search for all the integral points inside the circle and on this lattice.
This function define a lattice  with fundamental volume q. Tested!
-}

lattice :: (EuclideanDomain a, Ord a) => a -> a -> a -> (Vector a, Vector a)
lattice a b q 
	| euclid_mod b q /= 0 = (Vector 0 q, Vector 1 c)
	|otherwise = (Vector q 0, Vector 0 1)
	where 
		c = euclid_mod ((-1)* a * (inv' q b)) q
		

	
{-
This function reduce a given vector inside an almost orthogonal fundamental domain. Tested!
-}
roundbasis :: EuclideanDomain a => Vector a -> Vector a -> Vector a -> Vector a
roundbasis (Vector x y) (Vector a b) (Vector c d) = Vector r s
	where r = x - (a*z1 + c*z2)
	      s = y - (b*z1 + d*z2)
	      z1= euclid_div (d*x - c*y) (a*d-b*c)
	      z2= euclid_div (a*y - b*x) (a*d-b*c)



{-
This function gives us a solution mod q for t1*a + t2*b = (n- a^2 - b^2 )/2q mod q. Tested! 
-}	

modsol :: EuclideanDomain a => a -> a -> a -> a-> Vector a
modsol a b n q 
	| euclid_mod g q==0 = Vector 0 0
	| euclid_mod a q /= 0 = Vector r 0	
	| otherwise = Vector 0 s
	where 
		r = euclid_mod ((inv' q (2*a))*g) q
		s = euclid_mod ((inv' q (2*b))*g) q
		g = euclid_div (n - a*a - b*b) q 											
	
{- 
This gives us the center of a circle for potential points in (t1, t2) coordinates. Tested!
-}											

center :: (EuclideanDomain a, Ord a, Integral a) => a -> a -> a -> a -> Vector a
center a b n q = roundbasis v v1 v2 
	where 
		v = modsol a b n q
		(v1, v2) = gaussreduction $ lattice a b q 
		
{- This is the distance function for points. Tested! -}
dist2 :: (EuclideanDomain a, Ord a, Integral a) => Vector a -> Vector a -> a
dist2 (Vector x y) (Vector a b) = (x-a)*(x-a) + (y-b)*(y-b)


normm :: Num a => Vector a -> a 
normm (Vector x y) = (x*x) + (y*y)  



{-
Given a point and a circle, this function check whether the point is inside the circle or not. Tested!
-}
check :: (EuclideanDomain a, Ord a, Integral a) => Vector a -> Vector a -> a -> Bool 
check v1 v2 r2 = (dist2 v1 v2) <= r2	

checkq :: (EuclideanDomain a, Integral a) => a -> a -> a -> a -> Bool
checkq a b n q = (r/=0)
	where 
		r=euclid_mod (n - (a*a)- (b*b)) q 


{- In this module we list all the available points (t1, t2) such that (qt1+a) (qt2+b) is inside the circle and satisfy 
the congruence condition mod modd. The order of points start from the center and then we move
the center by translation with elements inside the lattice. Tested! -}



{-
In fact we can put sqrt 3 *(euclid_div n l1)+5. This improved it alot! Tested!
-}

listpoint :: Integer -> Integer -> Integer -> Integer -> [Vector Integer]
listpoint a b n modd 
	|checkq a b n modd = []
	|(a/=(euclid_mod a modd) || b/=(euclid_mod b modd))= listpoint (euclid_mod a modd) (euclid_mod b modd) n modd
	|otherwise  = [u| x <-[(-t1)..t1], y<-[(-t2)..t2], let u = ((v1 `vectMult` (x*modd )) `vplus` (v2 `vectMult` (y*modd)) `vplus` v), check o u n]
					where
						v = ((center a b n modd) `vectMult` modd) `vplus` (Vector a b) 		
						(v1, v2) = gaussreduction $ lattice a b modd
						o = Vector 0 0
						t1 = min p1 1000
						t2 = min p2 1000
						p1 = 3 *(round $ sqrt $ fromInteger $ euclid_div n l1)+5
						p2 = 3 *(round $ sqrt $ fromInteger $ euclid_div n l2)+5
						l1=modd*modd*(dist2 v1 o)
						l2=modd*modd*(dist2 v2 o)

{-
We want to add a condition on the parity of (a,b)

a is odd and b is even!
-}

{-
Given \epsilon, a, b and q, this function returns a list of integral lifts
(x,y,z,w) such that

x^2+y^2+z^2+w^2=m

where

x=a mod q
y=b mod q
z=w=0 mod q

m0=q^3 + a^2+b^2
mt= tq+m0

-}
{-
listm :: Integer -> Integer -> Integer->Floating -> [(ZComplex, Integer)]
-}

listm a b q e  = [(vectoCp u,k)| k<-[0..],  u<-(listpoint a' b' (m+k*q) q) ]
	where
	m=m0+(e*q^3)
	m0= euclid_mod (a'^2+b'^2) q
	a'= euclid_mod a q
	b'= euclid_mod b q
-- --------------------------------------
-- this function returns a list of possible integral lifts
-- ----------------------------------------	


  
diophantinem g q a b e effort = (uU, candidate_info) where
  candidates = listm a b q e 
  (uU, candidate_info) = first_solvable [] g candidates 
  first_solvable candidate_info g [] = error "gridsynth: internal error: finite list of candidates?"
  first_solvable candidate_info g ((u, tcount) : us) = case answer_t of
    Just (Just t) -> ((u, (qnum*t) ), ((u, tcount, Success) : candidate_info))
    Just Nothing -> first_solvable ((u, tcount, Fail) : candidate_info) g2 us
    Nothing -> first_solvable ((u, tcount, Timeout) : candidate_info) g2 us
    where
      (g1, g2) = split g
      m0 = euclid_mod (a^2+b^2) q
      m = m0+(e*q^3)
      qnum= fromIntegral q
      xi = fromIntegral(div ((m+tcount*q) - nmm u) (q*q))
      answer_t = run_bounded effort $ dioph_int_assoc g1 xi
     

{-
diophantine_approx g q a b p effort = (uU, candidate_info) where
  candidates = list_p g a b p q effort
  (uU, candidate_info) = first_solvable [] g candidates 
  first_solvable candidate_info g [] = error "gridsynth: internal error: finite list of candidates?"
  first_solvable candidate_info g ((u, tcount) : us) = case answer_t of
    Just (Just t) -> ((u, (qnum*t) ), ((u, tcount, Success) : candidate_info))
    Just Nothing -> first_solvable ((u, tcount, Fail) : candidate_info) g2 us
    Nothing -> first_solvable ((u, tcount, Timeout) : candidate_info) g2 us
    where
      (g1, g2) = split g
      qnum= fromIntegral q
      q2=q*q
      xi = fromIntegral(div ((p^tcount) - nmm u) q2)
      answer_t = run_bounded effort $ dioph_int_assoc g1 xi

-}
	
	
	
	
	
	
		













{-
This returns an infinite list of possible points ordered by k where n=p^k.
The test is OK.
There is a possible improvment on using power_mod function here.

Change the output into a Maybe Zcomplex type and also put the congruence condition mod 2 on the coordinates. a is odd and b is even!
-}

list_p :: (RandomGen t1 )=> t1-> Integer -> Integer -> Integer -> Integer->Int -> [(ZComplex, Integer)]
list_p g a b p q effort
 |((qresidue (a*a+b*b) q) ==1 && (qresidue p q ==1)) = [(vectoCp u,k)| k<-[0..],  u<-(listpoint (a'*(power_mod p' k q)) (b'*(power_mod p' k q)) (p^k) q) ]
 |((qresidue (a*a+b*b) q) ==1 && (qresidue p q ==(-1)))= [(vectoCp u,2*k)| k<-[0..],  u<-(listpoint (a'*(power_mod p k q)) (b'*(power_mod p k q)) (p^(2*k)) q) ]
 |((qresidue (a*a+b*b) q) ==(-1) && (qresidue p q ==(-1)))= [(vectoCp u,2*k+1)| k<-[0..],  u<-(listpoint (a''*(power_mod p k q)) (b''*(power_mod p k q)) (p^(2*k+1)) q) ]
 |otherwise = []	
 where 
 	(a',b') = (proj g a b 1 q)
 	(a'',b'') = (proj g a b p q)
 	p'=fromJust $ run_bounded effort $ root_mod g q p
 
 
{-Ginve a number n and a prime number q this function compute the quadratic reside of n mod q. Tested!    -}


qresidue :: Integer -> Integer -> Integer
qresidue n q 
  | n==1 = 1
  |((abs n) >q) = qresidue (euclid_mod n q) q
  |((euclid_mod n 4) == 0)   = qresidue (euclid_div n 4) q
  |((euclid_mod n 2) == 0)   = (qresidue (euclid_div n 2) q)* (m q)
  |otherwise = qresidue (q* (l n q)) n
		




{- Tested!
This module solve the projective version of the strong approximation. Given two parameters (a,b)
we will find "c" such that c^2 *(a^2 + b^2) is congruent to n=p^h mod q. It would be desirable if you can 
modify the answer into Maybe Integer and carry out the computation in this setup!

Maybe out put!
-}


proj :: (RandomGen t1 )=> t1-> Integer -> Integer -> Integer-> Integer -> (Integer,Integer)
proj g a b n q = (a', b')
	where
		a' = euclid_mod (a*c) q
		b' = euclid_mod (b*c) q
		u = fromJust $ inv_mod  q (a^2+ b^2)
		c = fromJust $ run_bounded 20 $ root_mod g q (u*n)



{-
Tested! Jacobi symbol mod 8
-}
m :: (Num a, EuclideanDomain a1) => a1 -> a
m q
 | (euclid_mod q 8 ==1 || euclid_mod q 8 ==7 ) = 1
 | (euclid_mod q 8 ==3 || euclid_mod q 8 ==5 ) = (-1)		 
 
 
 
{-Tested!
Jacobi symbol of 2 mod q.
-} 
 
l :: Integer -> Integer -> Integer
l n q
 | ((euclid_mod (qq*nn) 2) ==1) = (-1) 
 | ((euclid_mod (qq*nn) 2) ==0) = 1
 where
  qq=euclid_div (q-1) 2
  nn=euclid_div (n-1) 2 
 




{-
So far we implemented a code such that for every given 

Input:

a b n q

it returns a list of possible points (x,y) such that

x = a mod (q)
y = b mod (q),

q^2 | n - x^2 - y^2

n - x^2 - y^2 >0

We call this list by "raw_candidates".

let's write 

z = x+iy

we want to find w= s+it such that

q^2 | w^2

and z^2 + w^2 =n.

This part has been done in Ross and Selinger's code. we follow closely their method.
The idea is to define a StepComp class for handling random algorithm in case we won't
end up in an infinite loop. We also define  

Maybe a

class to handle the exceptions. The following function use the StepComp class to list the 
number of tries and status of each try. 
-}






















{-
here are the analogous parameters:

q ---- prec
a b -- theta
p --- archimedean 
effort- effort

-}

{-
The raw_candidates list is given by the application of list_p. It returns a list of Zcomplex (x+iy,k).
we need to find the possible complement to these vectors.
-}

{-
This is the main function of this module. This function check whether one can complete the raw candidates into 
a actual integral vector which satisfy the relevant congruence condition mod q. 
-}

diophantine_approx
  :: (RandomGen t1) =>
     t1
     -> Integer
     -> Integer
     -> Integer
     -> Integer
     -> Int
     -> ((Cplx Integer, ZComplex), [(Cplx Integer, Integer, DStatus)])
     
diophantine_approx g q a b p effort = (uU, candidate_info) where
  candidates = list_p g a b p q effort
  (uU, candidate_info) = first_solvable [] g candidates 
  first_solvable candidate_info g [] = error "gridsynth: internal error: finite list of candidates?"
  first_solvable candidate_info g ((u, tcount) : us) = case answer_t of
    Just (Just t) -> ((u, (qnum*t) ), ((u, tcount, Success) : candidate_info))
    Just Nothing -> first_solvable ((u, tcount, Fail) : candidate_info) g2 us
    Nothing -> first_solvable ((u, tcount, Timeout) : candidate_info) g2 us
    where
      (g1, g2) = split g
      qnum= fromIntegral q
      q2=q*q
      xi = fromIntegral(div ((p^tcount) - nmm u) q2)
      answer_t = run_bounded effort $ dioph_int_assoc g1 xi


		






nmm :: Num a => Cplx a -> a
nmm (Cplx a b) = a*a + b*b 


{-
The problem in the above function is when we write an integer in Z(sqrt{2}) as sum of two squars.
*Quantum.Synthesis.Diophantine> run $ dioph_int_assoc g 3
Just (Omega 1 0 1 1)

for example 3=(2+sqrt)
We have some options to solve this problem.

1) Given a number that you want to write as sum of two squares. 
Namely, 

(p^k - nmm u)

First, we factor this number into primes by the 

find_factor 

At each step we check wether the prime factor is 

4k+1

if yes, we apply the diophantine function to write it as sum of two squares.



2) the diophantine function which is implemented for Z[\sqrt{2}] for gaussian intergers.
We basically implement the continoued fraction algorithem in this case. The algorithm that
Sarnak asked me to read.


3) Apply diophantine function as it is and then check wether the omega^3 or omega coefficients are zero?

First check wether every prime number of the form 4k+1 is representable as sum of two squares.

Yes, the function works perfectly fine on primes which are of form 4k+1.

The first solution sounds more resonable than the rest!
-}


{-
we rewrite diophantine function so that it check a number is representable by sum of two squares. 
-}


{-
The best solution is to rewrite the diophantine function over Z with an image inside Z[i].

1) I learn very well Haskell and also the algorithm for writing a number as a sum of two squares.
-}

{-This part of algorithm is very fast! writing a number as sum of two squares-}

-- ----------------------------------------------------------------------
-- * Implementation details

-- ----------------------------------------------------------------------
-- ** Case: ξ is an integer                                 

-- | Given an integer /n/ ∈ ℤ, attempt to find /t/ ∈ ℤ[i] such that
-- /t/[sup †]/t/ ~ /n/, or return 'Nothing' if no such /t/ exists.
-- 
-- This function is optimized for the case when /n/ is prime, and
-- succeeds in an expected number of 2 ticks in this case.  If /n/ is
-- not prime, this function probably diverges.

{-
This function works fine! retune p= 4k+1 as sum of two squares.
-}
dioph_int_assoc_prime :: (RandomGen g) => g -> Integer -> StepComp (Maybe ZComplex)
dioph_int_assoc_prime g n
  | n < 0 = dioph_int_assoc_prime g (-n)
  | n == 0 = return (Just 0)
  | n == 2 = return (Just (1+i))
  | n_mod_4 == 1 = do
    h <- root_of_negative_one g n
    let t = euclid_gcd (fromInteger h+i) (fromInteger n) :: ZComplex
    assert (adj t * t == fromInteger n) $ return (Just t)
  | n_mod_4 == 3 = 
    return Nothing
  where
    n_mod_4 = n `mod` 4
    
  
    
    
{-
Tested

-}

-- | euclid_mod n 2 == 0 = fmap (( (1 +i) ::ZComplex)*) (dioph_int_assoc g (euclid_div n 2))
    
-- | Given an integer /n/ ∈ ℤ, find /t/ ∈ ℤ[i] such that /t/[sup †]/t/
-- ~ /n/, if such /t/ exists, or return 'Nothing' if no such /t/
-- exists. 
-- 
-- This function alternately calls 'dioph_int_assoc_prime' and
-- attempts to factor /n/. Therefore, it will eventually succeed;
-- however, the runtime depends on how hard it is to factor ξ.
dioph_int_assoc :: (RandomGen g) => g -> Integer -> StepComp (Maybe ZComplex)
dioph_int_assoc g n
  | n < 0 = dioph_int_assoc g (-n)
  | n == 0 = return (Just 0)
  | n == 1 = return (Just 1)
  | euclid_mod n 4 == 0 = fmap (fmap (2*)) (dioph_int_assoc g (euclid_div n 4))
  | euclid_mod n 2 == 0 = fmap (fmap ((1 +i) *)) (dioph_int_assoc g (euclid_div n 2))
  | otherwise = interleave prime_solver factor_solver where
    interleave p f = do
      p <- subtask 4 p
      case p of 
        Done res -> return res
        _ -> do
          f <- subtask 1000 f
          case f of
            Done (a, k) -> do
              let b = n `div` a
              let (u, facs) = relatively_prime_factors a b
              forward (k `div` 2) $ dioph_int_assoc_powers g3 facs
            _ -> interleave p f
  
    (g1, g') = split g
    (g2, g3) = split g'
    prime_solver = dioph_int_assoc_prime g1 n 
    factor_solver = with_counter $ speedup 30 $ find_factor g2 n
    
    
    

    
-- | Given a factorization /n/ = /q/[sub 1][sup /k/[sub 1]]⋅…⋅/q/[sub
-- /m/][sup /k/[sub /m/]] of an integer /n/, where /q/[sub 1], …,
-- /q/[sub /m/] are pairwise relatively prime, find /t/ ∈ ℤ[ω] such
-- that /t/[sup †]/t/ ~ /n/, if such /t/ exists, or return 'Nothing'
-- if no such /t/ exists.
dioph_int_assoc_powers :: (RandomGen g) => g -> [(Integer, Integer)] -> StepComp (Maybe ZComplex)
dioph_int_assoc_powers g facs = do
  res <- parallel_list_maybe [dioph_int_assoc_power g (n,k) | (n,k) <- facs]
  case res of
    Nothing -> return Nothing
    Just sols -> return (Just (product sols))












-- | Given a pair of integers (/n/, /k/), find /t/ ∈ ℤ[ω] such that
-- /t/[sup †]/t/ ~ /n/[sup /k/], if such /t/ exists, or return
-- 'Nothing' if no such /t/ exists.
dioph_int_assoc_power :: (RandomGen g) => g -> (Integer, Integer) -> StepComp (Maybe ZComplex)
dioph_int_assoc_power g (n,k)
  | even k = return (Just (fromInteger (n^(k `div` 2))))
  | otherwise = do
    t <- dioph_int_assoc g n
    case t of
      Nothing -> return Nothing
      Just t -> return (Just (t^k))














		
{-  
This function factor a number into its primes

factor :: Integral a => a -> [a]
-}


{-
Given an integer n this function returns a Vector a b such that  n= a^2 + b^2 
-}

{-
sumsquare :: Integral a => a -> Vector Integer
sumsquare n = vectoCp z
	where
		g1= mkStdGen (32412442)
		z = fromJust $ run $ diophantine g1 m
		m = fromIntegral n 
-}

{-This functon return (b,d) coordinate of an element in Z[omega] as a Vector b d  -}

vectoCp ::Vector Integer -> ZComplex
vectoCp (Vector a b) = Cplx a b 

{-
  | a == 0 && c == 0  = Vector b d
  | otherwise = error "zroottwo_of_zomega: non-real value"
-} 

{-
diopapprox a b n modd = map (ff n modd ) (listpoint a b n modd)
-}

		
		
{-		
ff n modd v = (v , u )		
	where
		u = u1 `vectMult` modd
		u1= sumsquare $ div m m2
		m = n-(normm v)
		m2= modd*modd
-}


-- ----------------------------------------------------------------------------------
-- Implementation for LPS Ramanujan graphs
-- ----------------------------------------------------------------------------------




diophantineLPS
  :: (RandomGen t1) =>
     t1
     -> Integer
     -> Integer
     -> Integer
     -> Integer
     -> Int
     -> ((Cplx Integer, ZComplex), [(Cplx Integer, Integer, DStatus)])
     
diophantineLPS g q a b p effort = (uU, candidate_info) where
  candidates = list_pLPS g a b p q effort
  (uU, candidate_info) = first_solvable [] g candidates 
  first_solvable candidate_info g [] = error "gridsynth: internal error: finite list of candidates?"
  first_solvable candidate_info g ((u, tcount) : us) = case answer_t of
    Just (Just t) -> ((u, (qnum*t) ), ((u, tcount, Success) : candidate_info))
    Just Nothing -> first_solvable ((u, tcount, Fail) : candidate_info) g2 us
    Nothing -> first_solvable ((u, tcount, Timeout) : candidate_info) g2 us
    where
      (g1, g2) = split g
      qnum= fromIntegral q
      q2=q*q
      xi = fromIntegral(div ((p^tcount) - nmm u) q2)
      answer_t = run_bounded effort $ dioph_int_assoc g1 xi


		


























{-
These functions return (a,b,c,d) such that

a^2+b^2+c^2+d^2 = p^h

 ,  2|b,   2q|c,  2q|d.  

Moreover [a+bi and a-bi] should be congruent to a given pair projectively. 

-}





{-
In fact we can put sqrt 3 *(euclid_div n l1)+5. This improved it alot! Tested!
-}

listpointLPS :: Integer -> Integer -> Integer -> Integer -> [Vector Integer]
listpointLPS a b n modd 
	|checkq a b n (4*modd) = []
	|(a/=(euclid_mod a (2*modd)) || b/=(euclid_mod b (2*modd)))= listpointLPS (euclid_mod a (2*modd)) (euclid_mod b (2*modd)) n modd
	|otherwise  = [u| x <-[(-t1)..t1], y<-[(-t2)..t2], let u = ((v1 `vectMult` (x*2*modd )) `vplus` (v2 `vectMult` (y*2*modd)) `vplus` v), check o u n]
					where
						v = ((centerLPS a b n modd) `vectMult` (2*modd)) `vplus` (Vector a b) 		
						(v1, v2) = gaussreduction $ lattice a b modd
						o = Vector 0 0
						t1 = min p1 100
						t2 = min p2 100
						p1 = 3 *(round $ sqrt $ fromInteger $ euclid_div n l1)+5
						p2 = 3 *(round $ sqrt $ fromInteger $ euclid_div n l2)+5
						l1=modd*modd*(dist2 v1 o)
						l2=modd*modd*(dist2 v2 o)
						
						
						

{-
We want to add a condition on the parity of (a,b)

a is odd and b is even!
-}






{-
This returns an infinite list of possible points ordered by k where n=p^k.
The test is OK.
There is a possible improvment on using power_mod function here.

Change the output into a Maybe Zcomplex type and also put the congruence condition mod 2 on the coordinates. a is odd and b is even!
-}

list_pLPS :: (RandomGen t1 )=> t1-> Integer -> Integer -> Integer -> Integer->Int -> [(ZComplex, Integer)]
list_pLPS g a b p q effort
 |((qresidue (a*a+b*b) q) ==1 && (qresidue p q ==1)) = [(vectoCp u,k)| k<-[0..],  u<-(listpointLPS (a'*(power_mod p' k (2*q))) (b'*(power_mod p' k (2*q))) (p^k) q) ]
 |((qresidue (a*a+b*b) q) ==1 && (qresidue p q ==(-1)))= [(vectoCp u,2*k)| k<-[0..],  u<-(listpointLPS (a'*(power_mod p k (2*q))) (b'*(power_mod p k (2*q))) (p^(2*k)) q) ]
 |((qresidue (a*a+b*b) q) ==(-1) && (qresidue p q ==(-1)))= [(vectoCp u,2*k+1)| k<-[0..],  u<-(listpointLPS (a''*(power_mod p k (2*q))) (b''*(power_mod p k (2*q))) (p^(2*k+1)) q) ]
 |otherwise = []	
 where 
 
 	(a',b') = (projLPS g a b 1 q)	
 	(a'',b'') =(projLPS g a b p q)
 	p'=oddcong pp q
 	pp=fromJust $ run_bounded effort $ root_mod g q p
 	
 	
 	
oddcong:: Integer -> Integer -> Integer
oddcong a q 
	|odd (euclid_mod a q) = euclid_mod a q
	|even(euclid_mod a q) = q+ (euclid_mod a q)
	
evencong:: Integer -> Integer -> Integer
evencong a q 
	|even (euclid_mod a q) = euclid_mod a q
	|odd (euclid_mod a q) = q+(euclid_mod a q)
	
 
 
{-Ginve a number n and a prime number q this function compute the quadratic reside of n mod q. Tested!    -}





{- Tested!
This module solve the projective version of the strong approximation. Given two parameters (a,b)
we will find "c" such that c^2 *(a^2 + b^2) is congruent to n=p^h mod q. It would be desirable if you can 
modify the answer into Maybe Integer and carry out the computation in this setup!

Maybe out put!
-}

{-
Tested!
-}
projLPS :: (RandomGen t1 )=> t1-> Integer -> Integer -> Integer-> Integer -> (Integer,Integer)
projLPS g a b n q = (a', b')
	where
	(a',b') = (oddcong aaa q, evencong bbb q)
 	(aaa,bbb) = (proj g a b n q)

{-
We are solving the following diophantine equation.


(2t_1*q +a)^2 + (2t_2*q +b )^2 +4q^2 (t_3^2 + t_4^2) = n

From this we obtain

a*t_1 + b*t_2 = \frac{n-a^2-b^2}{4*q}  mod q

The only differnce is division by 4 instead of 2. So 

We modify modsol and center functions to 

modsolLPS and centerLPS to obtain t_1 and t_2 with

the smallest congruence condition.

-}

centerLPS :: (EuclideanDomain a, Ord a, Integral a) => a -> a -> a -> a -> Vector a
centerLPS a b n q = roundbasis v v1 v2 
	where 
		v = modsolLPS a b n q
		(v1, v2) = gaussreduction $ lattice a b q 



modsolLPS :: EuclideanDomain a => a -> a -> a -> a-> Vector a
modsolLPS a b n q 
	| euclid_mod g q==0 = Vector 0 0
	| euclid_mod a q /= 0 = Vector r 0	
	| otherwise = Vector 0 s
	where 
		r = euclid_mod ((inv' q a)*g) q
		s = euclid_mod ((inv' q b)*g) q
		g = euclid_div (n - a*a - b*b) (4*q) 											
	






-- -------------------------------------------------------------------
-- Path implementation 
-- -------------------------------------------------------------------


mult :: (ZComplex,ZComplex)->(ZComplex,ZComplex) -> (ZComplex,ZComplex)
mult (z,w) (z1,w1) = (z*z1-(w*(adj w1)), z*w1+(w*(adj z1)))



divs5 :: (ZComplex,ZComplex)->Bool
divs5 (Cplx a b,Cplx c d)= ((mod a 5)==0) && ((mod b 5)==0) && ((mod c 5)==0) && ((mod d 5)==0)

div5 :: (ZComplex,ZComplex)->(ZComplex,ZComplex)
div5 (Cplx a b,Cplx c d) = (Cplx (euclid_div a 5) (euclid_div b 5),Cplx (euclid_div c 5) (euclid_div d 5))


decompose :: (ZComplex,ZComplex) -> String
decompose (z,w)
	| divs5 (mult (Cplx 1 2, 0) (z,w) )= " Vz^{-1} "++ (decompose $ div5 $mult (Cplx 1 2, 0) (z,w) )
	| divs5 (mult (Cplx 1 (-2), 0) (z,w))= " Vz "++(decompose $ div5 $ mult (Cplx 1 (-2), 0) (z,w))
	| divs5 (mult (1, Cplx 2 0) (z,w) )= " Vy^{-1} "++(decompose $ div5 $ mult (1, Cplx 2 0) (z,w))
	| divs5 (mult (1, Cplx (-2) 0) (z,w) )= " Vy "++(decompose $ div5 $ mult (1, Cplx (-2) 0) (z,w))
	| divs5 (mult (1, Cplx 0 2) (z,w) )= " Vx^{-1} "++(decompose $ div5 $ mult (1, Cplx 0 2) (z,w))
	| divs5 (mult (1, Cplx 0 (-2)) (z,w) )= " Vx "++(decompose $ div5 $ mult (1, Cplx 0 (-2)) (z,w))
	| otherwise = []






-- -------------------------------------------------------------------
-- Path to global lift 
-- -------------------------------------------------------------------


globallift :: String -> (ZComplex,ZComplex)
globallift a = global aa
 where
  aa=splitOn "  " b
  b=init $ tail $ a
  


global :: [String] -> (ZComplex,ZComplex)
global a 
    | a==[] =(1,0)
	|head a == "Vz" = mult (Cplx 1 2, 0) $ global $ tail a
	|head a == "Vz^{-1}" = mult (Cplx 1 (-2), 0) $ global $ tail a
	|head a == "Vy" =  mult (1, Cplx 2 0) $ global $ tail a
	|head a == "Vy^{-1}" = mult (1, Cplx (-2) 0) $ global $ tail a
	|head a == "Vx" =  mult (1, Cplx 0 2) $ global $ tail a
	|head a == "Vx^{-1}" =  mult (1, Cplx 0 (-2)) $ global $ tail a
	|otherwise = (1,0)





