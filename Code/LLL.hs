-- In this module we give an LLL algorithm. 
-- Input: A lattice spans by two integral vectors [v1,v2]. 
-- Output a new basis [u1, u2]. This is an almost orthogonal basis for the lattice.

module LLL where
import Quantum.Synthesis.EuclideanDomain
import Data.Maybe


{-This is a safe function. This gives us a reduced basis for the two dimensional lattice-}

gaussreduction :: (Ord t,  EuclideanDomain t, Integral t) => (Vector t, Vector t) -> (Vector t, Vector t)
gaussreduction (v1,v2)
	| abss (v1 `scalarMult` v1) > abss (v2 `scalarMult` v2)  = gaussreduction (v2,v1)
	| 2*(abss (v1 `scalarMult` v2)) > abss (v1 `scalarMult` v1) = gaussreduction (v1, v2 `vmin` (vectMult v1 gramco) )
	| 2*(abss (v1 `scalarMult` v2)) <= abss (v1 `scalarMult` v1)  = (v1,v2)
	where gramco = euclidd (v1 `scalarMult` v2) (v1 `scalarMult` v1) 
	
{- round function -}
euclidd :: (Ord t,  EuclideanDomain t, Integral t) => t -> t -> t
euclidd a b = rounddiv a b


{-
	| 2*(abss(euclid_mod a b)) <= b = euclid_div a b
	| (2*(euclid_mod a b) > b) && (b>0) =(euclid_div a b)-1
	| (2*(euclid_mod a b)) < 0) && (b>0) = (euclid_div a b)+1
	|(2*(euclid_mod a b)) < 0) && (b<0) = (euclid_div a b)-1
	|(2*(euclid_mod a b) > 0 ) && (b<0)= (euclid_div a b)+1
-}

{-
this is a safe function
gaussreduction :: (Vector R, Vector R) -> (Vector R, Vector R)
gaussreduction (v1,v2) 
	| v1 <.> v1 > v2 <.> v2  = gaussreduction (v2,v1)
	| v1 <.> v2 >= v1 <.> v1 = gaussreduction (v1, v2 - gramco * v1)
	| v1 <.> v2 < v1 <.> v1  = (v1,v2)
	where gramco = fromIntegral $ round $ (v1 <.> v2)/ (v1 <.> v1) 	
-}
	
abss :: (Ord t,  EuclideanDomain t, Integral t) => t -> t
abss a 
	| a>=0 = a
	| a<0  = (-a) 





	

data Vector a = Vector a a deriving (Show)  
  
vplus :: (Num t, EuclideanDomain t, Integral t) => Vector t -> Vector t -> Vector t  
(Vector i j) `vplus` (Vector l m) = Vector (i+l) (j+m)  

vmin :: (Num t, EuclideanDomain t, Integral t) => Vector t -> Vector t -> Vector t 
(Vector i j) `vmin` (Vector l m) = Vector (i-l) (j-m)
  
vectMult :: (Num t, EuclideanDomain t, Integral t) => Vector t -> t -> Vector t  
(Vector i j) `vectMult` m = Vector (i*m) (j*m)  
  
scalarMult :: (Num t, EuclideanDomain t, Integral t) => Vector t -> Vector t -> t  
(Vector i j) `scalarMult` (Vector l m) = i*l + j*m






{-
data Matrix a = Matrix (Vector a) (Vector a) (Show)

solveint :: (EuclideanDomain t) => Matrix t -> Vector t -> Vector t
solveint (Matrix (Vector a b) (Vector c d)) (Vector x y) = Vector x y
-}


{-
where r = euclid_div (d*x - c*y) (a*d-b*c)
	  s = euclid_div (a*y - b*x) (a*d-b*c )
-}		 
						
						  
								
