\contentsline {section}{\tocsection {}{1}{Introduction}}{1}{section.1}
\contentsline {subsection}{\tocsubsection {}{1.1}{Motivtion}}{1}{subsection.1.1}
\contentsline {subsection}{\tocsubsection {}{1.2}{Reduction to strong approximation on the sphere}}{4}{subsection.1.2}
\contentsline {subsection}{\tocsubsection {}{1.3}{Complexity of strong approximation on the sphere}}{5}{subsection.1.3}
\contentsline {subsection}{\tocsubsection {}{1.4}{Quantitative bounds on the size of the output}}{6}{subsection.1.4}
\contentsline {subsection}{\tocsubsection {}{1.5}{Further motivations and techniques}}{7}{subsection.1.5}
\contentsline {subsection}{\tocsubsection {}{}{Acknowledgements}}{7}{section*.2}
\contentsline {section}{\tocsection {}{2}{NP-Completeness}}{7}{section.2}
\contentsline {section}{\tocsection {}{3}{Algorithm}}{10}{section.3}
\contentsline {subsection}{\tocsubsection {}{3.1}{Proof of Theorem\nonbreakingspace \ref {diagliftt}}}{10}{subsection.3.1}
\contentsline {subsection}{\tocsubsection {}{3.2}{Distance of diagonal vertices from the identity}}{13}{subsection.3.2}
\contentsline {subsection}{\tocsubsection {}{3.3}{Algorithm for the diagonal decomposition}}{15}{subsection.3.3}
\contentsline {section}{\tocsection {}{4}{Numerical results}}{17}{section.4}
\contentsline {subsection}{\tocsubsection {}{4.1}{Diagonal approximation with V-gates}}{17}{subsection.4.1}
\contentsline {subsection}{\tocsubsection {}{4.2}{Lower bound on the diameter of LPS Ramanujan graphs }}{19}{subsection.4.2}
\contentsline {section}{\tocsection {}{}{References}}{19}{section*.3}
